package com.example.calculator;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    Button Btn0, Btn1, Btn2, Btn3, Btn4, Btn5, Btn6, Btn7, Btn8, Btn9, divBtn, mulBtn, subBtn, addBtn, clearBtn, ansBtn;
    TextView textView, historyTextView;
    float num;
    String op;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView =  findViewById(R.id.textView);
    historyTextView=findViewById(R.id.historyTextView);
        Btn0 =      findViewById(R.id.Btn0);
        Btn1 =      findViewById(R.id.Btn1);
        Btn2 =      findViewById(R.id.Btn2);
        Btn3 =      findViewById(R.id.Btn3);
        Btn4 =      findViewById(R.id.Btn4);
        Btn5 =      findViewById(R.id.Btn5);
        Btn6 =      findViewById(R.id.Btn6);
        Btn7 =      findViewById(R.id.Btn7);
        Btn8 =      findViewById(R.id.Btn8);
        Btn9 =      findViewById(R.id.Btn9);
        divBtn =    findViewById(R.id.divBtn);
        mulBtn =    findViewById(R.id.mulBtn);
        subBtn =    findViewById(R.id.subBtn);
        addBtn =    findViewById(R.id.addBtn);
        clearBtn =  findViewById(R.id.clearBtn);
        ansBtn  =   findViewById(R.id.ansBtn);

        Btn0.setOnClickListener(this);
        Btn1.setOnClickListener(this);
        Btn2.setOnClickListener(this);
        Btn3.setOnClickListener(this);
        Btn4.setOnClickListener(this);
        Btn5.setOnClickListener(this);
        Btn6.setOnClickListener(this);
        Btn7.setOnClickListener(this);
        Btn8.setOnClickListener(this);
        Btn9.setOnClickListener(this);

        divBtn.setOnClickListener(this);
        mulBtn.setOnClickListener(this);
        subBtn.setOnClickListener(this);
        addBtn.setOnClickListener(this);
        clearBtn.setOnClickListener(this);

        ansBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.Btn0:
                    textView.setText(textView.getText()+"0");
                break;
            case R.id.Btn1:
                textView.setText(textView.getText()+"1");
                break;
            case R.id.Btn2:
                textView.setText(textView.getText()+"2");
                break;
            case R.id.Btn3:
                textView.setText(textView.getText()+"3");
                break;
            case R.id.Btn4:
                textView.setText(textView.getText()+"4");
                break;
            case R.id.Btn5:
                textView.setText(textView.getText()+"5");
                break;
            case R.id.Btn6:
                textView.setText(textView.getText()+"6");
                break;
            case R.id.Btn7:
                textView.setText(textView.getText()+"7");
                break;
            case R.id.Btn8:
                textView.setText(textView.getText()+"8");
                break;
            case R.id.Btn9:
                textView.setText(textView.getText()+"9");
                break;

            case R.id.divBtn:
                        num = Integer.parseInt(textView.getText().toString());
                        historyTextView.setText(num+"/");
                        op="/";
                        textView.setText("");
                break;
            case R.id.addBtn:
                        num = Integer.parseInt(textView.getText().toString());
                        historyTextView.setText(num+"+");
                        op="+";
                        textView.setText("");
                break;
            case R.id.subBtn:
                        num = Integer.parseInt(textView.getText().toString());
                        historyTextView.setText(num+"-");
                        op="-";
                        textView.setText("");
                break;
            case R.id.mulBtn:
                        num = Integer.parseInt(textView.getText().toString());
                        historyTextView.setText(num+"*");
                        op="*";
                        textView.setText("");
                break;

            case R.id.ansBtn:
                        historyTextView.setText(historyTextView.getText()+""+textView.getText()+"");
                        float Answer=0;
                        int val2 = Integer.parseInt(textView.getText().toString());
                        switch (op){
                            case "+":
                                    Answer = num+val2;
                                break;
                            case "-":
                                    Answer = num-val2;
                                break;
                            case "/":
                                    Answer = num/val2;
                                break;
                            case "*":
                                    Answer = num*val2;
                                break;
                        }
                        textView.setText(Answer+"");

                break;
            case R.id.clearBtn:
                textView.setText("");
                historyTextView.setText("");
                break;
        }
    }
}